import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

dti = pd.date_range("2018-01-01 08:00", periods=13*60/5, freq="5min")
count = np.random.rand(len(dti))

i = 0
output = []
for item in count:
    output.append(item*(-i**2+len(count)*i))
    i += 1


plt.plot(dti, output)
plt.show()
